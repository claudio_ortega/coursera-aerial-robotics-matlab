function [ u1, u2 ] = controller(time, state, des_state, params)
%CONTROLLER  Controller for the planar quadrotor
%
%   state: The current state of the robot with the following fields:
%   state.pos = [y; z], state.vel = [y_dot; z_dot], state.rot = [phi],
%   state.omega = [phi_dot]
%
%   des_state: The desired states are:
%   des_state.pos = [y; z], des_state.vel = [y_dot; z_dot], des_state.acc =
%   [y_ddot; z_ddot]
%
%   params: robot parameters

%   Using these current and desired states, you have to compute the desired
%   controls

kp_y = 100.0;
kd_y = 10.0;

kp_z = 20.0;
kd_z = 2.0;

kp_phi = 50.0;
kd_phi = 3.0;

u1 = params.mass * ( params.gravity + des_state.acc(2) + kd_z * ( des_state.vel(2) - state.vel(2) ) + kp_z * ( des_state.pos(2) - state.pos(2) ) );
phi_c = ( - 1.0 / params.gravity ) * ( des_state.acc(1) + kd_y * ( des_state.vel(1) - state.vel(1) ) + kp_y * ( des_state.pos(1) - state.pos(1) ) );

phi_c_dot = 0.0;  

u2 = kd_phi * ( phi_c_dot - state.omega(1) ) + kp_phi * ( phi_c - state.rot(1) );

end

