addpath('utils2');


% auxiliars for assignment week 4
v1 = [ 1; 2; 3];
A = Hat ( v1 );
v2 = HatInverse( A );
areAlmostEqual( v1, v2, 1e-6 );

% quiz week 4
tn = versor ( [sind(30) * cosd(45); sind(30) * sind(45); cosd(30)] );
B = YawAndThrustToRot_ZXY( degtorad(45), tn );

% body to world coordinates
RPYtoRot_ZXY ( 0, 0, degtorad(45) );

u = [ 1; 0; 0 ];

canProduce = canProduceOrthonormalVersors ( u )

[a,b,c] = produceOrthonormalVersors ( u )

a_x_b = cross ( a, b )
b_x_c = cross ( b, c )
c_x_a = cross ( c, a )

a_dot_b = dot ( a, b )
b_dot_c = dot ( b, c )
c_dot_a = dot ( c, a)
