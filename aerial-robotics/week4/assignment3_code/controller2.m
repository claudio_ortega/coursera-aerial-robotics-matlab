function [F, M] = controller2(t, state, des_state, params)

%   Controller for the quadrotor **** from handout
%
%   state: The current state of the robot with the following fields:
%   state.pos = [x; y; z], state.vel = [x_dot; y_dot; z_dot],
%   state.rot = [phi; theta; psi], state.omega = [p; q; r]
%
%   des_state: The desired states are:
%   des_state.pos = [x; y; z], des_state.vel = [x_dot; y_dot; z_dot],
%   des_state.acc = [x_ddot; y_ddot; z_ddot], des_state.yaw,
%   des_state.yawdot
%
%   params: robot parameters
%   Using these current and desired states, you have to compute the desired
%   controls
%


kp_r = 20.0;
kd_r = 6.0;

kp_phi = 10.0;
kd_phi = 1.0;

kp_theta = 10.0;
kd_theta = 1.0;

kp_psi = 10.0;
kd_psi = 1.0;


% around formula 17
rT = [ des_state.pos(1); des_state.pos(2); des_state.pos(3) ];
rT_dot = [ des_state.vel(1); des_state.vel(2); des_state.vel(3) ];
rT_dot_dot = [ des_state.acc(1); des_state.acc(2); des_state.acc(3) ];

rP = [ state.pos(1); state.pos(2);, state.pos(3) ];
rP_dot = [ state.vel(1); state.vel(2); state.vel(3) ];


% determine ep
if canProduceOrthonormalVersors ( rT_dot )
    % project rT - rP on plane normal to trajectory
    [t,n,b] = produceOrthonormalVersors( rT_dot );
    ep = dot ( rT - rP, n ) * n + dot ( rT - rP, b ) * b;

    % this works better than using the projection, is it worth it?
    ep = rT - rP;
else
    % take rT - rP as is
    ep = rT - rP;

end

% determine ev
ev = rT_dot - rP_dot;

% determine phi, theta
psiT = des_state.yaw(1);
psiT_dot = des_state.yawdot(1);

% formula 17
r_des_dot_dot = rT_dot_dot + kd_r * ev + kp_r * ep;

% formulas 14 a,b
phi_des = ( 1.0 / params.gravity ) * ( r_des_dot_dot(1) * sin( psiT ) - r_des_dot_dot(2) * cos( psiT ) );
theta_des = ( 1.0 / params.gravity ) * ( r_des_dot_dot(1) * cos( psiT ) + r_des_dot_dot(2) * sin( psiT ) );

% formulas 16 a,b
psi_des = psiT;
r_des = psiT_dot;

phi = state.rot(1);
theta = state.rot(2);
psi = state.rot(3);

% formulas 15 a,b
p_des = 0;
q_des = 0;
p = state.omega(1);
q = state.omega(2);
r = state.omega(3);

% formula 13
u1 = params.mass * ( params.gravity + r_des_dot_dot(3) );

% formulas 10
u2 = kp_phi * ( phi_des - phi ) + kd_phi * ( p_des - p );
u3 = kp_theta * ( theta_des - theta ) + kd_theta * ( q_des - q );
u4 = kp_psi * ( psi_des - psi ) + kd_psi * ( r_des - r );

F = u1;
M = [u2;u3;u4];

end
