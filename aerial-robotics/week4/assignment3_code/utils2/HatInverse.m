function [v] = HatInverse( skewMatrix )
%

assert ( issymmetric ( skewMatrix, 'skew' ), strcat ( 'matrix is not skew symmetric', mat2str( skewMatrix ) ) )


a1 = -skewMatrix(2,3);
a2 =  skewMatrix(1,3);
a3 = -skewMatrix(1,2);

v = [a1; a2; a3];

end
