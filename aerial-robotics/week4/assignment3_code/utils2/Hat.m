function [skewMatrix] = Hat( vector )
%

skewMatrix = [ 0, -vector(3), vector(2); vector(3), 0, -vector(1); -vector(2), vector(1), 0 ];

assert ( issymmetric ( skewMatrix, 'skew' ), strcat ( 'matrix is not skew symmetric', mat2str( skewMatrix ) ) )

end
