function [d] = Diagonal( a11, a22, a33 )
%

d = [ a11, 0, 0; 0, a22, 0; 0, 0, a33 ];

end
