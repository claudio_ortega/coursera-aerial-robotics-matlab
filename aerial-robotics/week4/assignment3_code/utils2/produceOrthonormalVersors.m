function [t,n,b] = produceOrthonormalVersors ( x )

assert( canProduceOrthonormalVersors( x ), 'vector passed cannnot produce an orthonormal basis' );

checkIsAColumnarVector ( x )

t = versor ( x );

y1 = t + [ 0; 1; 0 ];
y2 = t + [ 0; 0; 1 ];

if norm ( y1 ) > norm ( y2 )
    y = y1;
else
    y = y2;
end

n = versor ( cross ( t, y ) );
b = versor ( cross ( t, n ) );

end
