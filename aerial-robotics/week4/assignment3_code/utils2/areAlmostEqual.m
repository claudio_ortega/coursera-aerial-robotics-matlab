function [t] = areAlmostEqual( v1, v2, diff )

t = norm( v1 - v2 ) < diff;

end
