function b = canProduceOrthonormalVersors( x )

checkIsAColumnarVector ( x )

threshold = 1e-6;

b = norm(x) > threshold;

end
