function [R] = YawAndThrustToRot_ZXY( yaw, thrust )
% RotToRPY_ZXY Produce a rotation matrix with the constrains:
% - rotates (0,0,1) into the passed thrust vector
% - the yaw angle in the ZXY rotation sequence is the one passed


psi = yaw;
thrustX = thrust(1);
thrustY = thrust(2);
% thrustZ is not needed, but should be used to test the result, at least
thrustZ = thrust(3);

A = [ cos(psi), sin(psi); sin(psi), -cos(psi) ];
IA = transpose ( A );  % transposing as A is orthogonal;

v = IA * [ thrustX; thrustY ];

theta = asin( v(1) );

sinOfPhi = v(2) / cos ( theta );
phi = asin ( sinOfPhi );

% RPYtoRot_ZXY returns a world-to-body rotation
% R should be a body to world Rot matrix, so we transpose
R = transpose ( RPYtoRot_ZXY ( phi, theta, psi ) );

deviation = norm( thrust - [ R(1,3); R(2,3); R(3,3) ] );

assert ( deviation < 1 );

end
