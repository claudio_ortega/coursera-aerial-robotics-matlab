function [vn] = versor(x)

vn = x / norm(x);

end
