function checkIsAColumnarVector( x )

[rows,columns] = size ( x );

assert ( columns == 1, 'not a columnar vector' )

end
