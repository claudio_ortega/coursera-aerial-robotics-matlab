function [F, M] = controller(t, state, des_state, params)

%   Controller for the quadrotor NOT from handout
%
%   state: The current state of the robot with the following fields:
%   state.pos = [x; y; z], state.vel = [x_dot; y_dot; z_dot],
%   state.rot = [phi; theta; psi], state.omega = [p; q; r]
%
%   des_state: The desired states are:
%   des_state.pos = [x; y; z], des_state.vel = [x_dot; y_dot; z_dot],
%   des_state.acc = [x_ddot; y_ddot; z_ddot], des_state.yaw,
%   des_state.yawdot
%
%   params: robot parameters
%   Using these current and desired states, you have to compute the desired
%   controls
%

%{
    Based on:
    - Non linear control - Slide 6
    - Geometric Tracking Control of a Quadrotor
%}


%%%%% -----
Kp = 1.0 * params.mass;
Kv = 10.0 * params.mass;
%%%%% -----

a3 = [ 0; 0; 1 ];
thrust = des_state.acc + Kv * ( des_state.vel - state.vel ) + Kp * ( des_state.pos - state.pos ) + ( params.mass * params.gravity * a3 );

R = transpose ( RPYtoRot_ZXY( state.rot(1), state.rot(2), state.rot(3) ) );
b3 = R * a3;


Rdes = YawAndThrustToRot_ZXY( des_state.yaw(1), versor( thrust ) );
eR = 0.5 * HatInverse ( transpose( Rdes ) * R - transpose( R ) * Rdes );

omega = [ state.omega(1); state.omega(2); state.omega(3) ];
eW = [ 0; 0; des_state.yawdot(1) ] - omega;

%%%%% ----- 
KRot = 0.01 * eye ( 3 );
KOmega = 0.01 * eye ( 3 );
%%%%% -----

u1 = dot ( thrust, b3 );
u2 = cross ( omega, params.I * omega ) - params.I * ( KRot * eR + KOmega * eW );

F = u1;
M = u2;

end
