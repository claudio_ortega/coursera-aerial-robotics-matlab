function plot3(YMatrix1)

% Create figure
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');

% Create multiple lines using matrix input to plot
plot1 = plot(YMatrix1,'Parent',axes1);
set(plot1(1),'DisplayName','Samples(:,1)');
set(plot1(2),'DisplayName','Samples(:,2)');
set(plot1(3),'DisplayName','Samples(:,3)');

box(axes1,'on');
